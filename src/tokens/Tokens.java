package tokens;

import java.util.Hashtable;

public class Tokens {
    static enum token {
        IDENT,
        NUMBER,
        CONST,
        VAR,
        PROCEDURE,
        CALL,
        BEGIN,
        END,
        IF,
        THEN,
        WHILE,
        DO,
        ODD,
        QMARK, //?
        EMARK, //!
        COMMA, //,
        EOS, //;
        EQUALS,//=
        COLONEQUALS,//:=
        HASH,//#
        LESSEQUALS, //<=
        GREATEREQUALS, //>=
        LESS, // <
        GREATER, // >
        PLUS, //+
        MINUS, //-
        MULTIPLY, //*
        DIVISION, // /
        LBRACKET, // (
        RBRACKET, // )
        DOT // .
    }

    public static Hashtable<String, token> getTokenMap() {
        Hashtable<String, token> tokensMap = new Hashtable<String, token>();
        tokensMap.put("ident", token.IDENT);
        tokensMap.put("number", token.NUMBER);
        tokensMap.put("const", token.CONST);
        tokensMap.put("var", token.VAR);
        tokensMap.put("procedure", token.PROCEDURE);
        tokensMap.put("call", token.CALL);
        tokensMap.put("begin", token.BEGIN);
        tokensMap.put("end", token.END);
        tokensMap.put("if", token.IF);
        tokensMap.put("then", token.THEN);
        tokensMap.put("while", token.WHILE);
        tokensMap.put("do", token.DO);
        tokensMap.put("odd", token.ODD);
        return tokensMap;
    }

    public static Hashtable<String, token> getCharTokenMap() {
        Hashtable<String, token> CharTokenMap = new Hashtable<String, token>();
        CharTokenMap.put("?", token.QMARK);
        CharTokenMap.put("!", token.EMARK);
        CharTokenMap.put(",", token.COMMA);
        CharTokenMap.put(";", token.EOS);
        CharTokenMap.put("=", token.EQUALS);
        CharTokenMap.put(":=", token.COLONEQUALS);
        CharTokenMap.put("#", token.HASH);
        CharTokenMap.put("<=", token.LESSEQUALS);
        CharTokenMap.put(">=", token.GREATEREQUALS);
        CharTokenMap.put("<", token.LESS);
        CharTokenMap.put(">", token.GREATER);
        CharTokenMap.put("+", token.PLUS);
        CharTokenMap.put("-", token.MINUS);
        CharTokenMap.put("*", token.MULTIPLY);
        CharTokenMap.put("/", token.DIVISION);
        CharTokenMap.put("(", token.LBRACKET);
        CharTokenMap.put(")", token.RBRACKET);
        CharTokenMap.put(".", token.DOT);
        return CharTokenMap;
    }

    public static boolean isMemberOfCharTokenMap(String line) {
        line = line.toLowerCase();
        if (getCharTokenMap().containsKey(line)) {
            return false;
        }/* else if (getCharTokenMap().containsKey(line.substring(0, 1))) { //Začátek
            return true;
        }*/ else if (getCharTokenMap().containsKey(line.substring(line.length() - 1))) { //Konec
            return true;
        } else {
            return false;
        }
    }

    public static boolean isMemberOfTokensMap(String line) {
        line = line.toLowerCase();
        Hashtable<String, token> allTokenMap = new Hashtable<String, token>();
        allTokenMap.putAll(getCharTokenMap());
        allTokenMap.putAll(getTokenMap());
        if (allTokenMap.containsKey(line)) {
            return true;
        } else {
            return false;
        }
    }

    public static token getCharToken(String line) {
        line = line.toLowerCase();
        if (getCharTokenMap().containsKey(line)) {
            return getCharTokenMap().get(line);
        } else {
            return null;
        }
    }

    public static token getToken(String line) {
        line = line.toLowerCase();
        if (getTokenMap().containsKey(line)) {
            return getTokenMap().get(line);
        } else {
            return null;
        }
    }

    public static String getCharFromCharTokenMap(String line) {
        line = line.toLowerCase();
        System.out.println("Testing inside: " + line);
        if (getCharTokenMap().containsKey(line)) {
            System.out.println("Full line: " + line);
            return line;
        } else if (getCharTokenMap().containsKey(line.substring(line.length() - 1))) {
            System.out.println("Char line end: " + line.substring(line.length() - 1));
            return line.substring(line.length() - 1);
        } else {
            return null;
        }

        //PRO PRVNÍ CHAR
          /*  if (getCharTokenMap().containsKey(line.substring(0,1))) {
            System.out.println("Char line 0: " + line.substring(0,1));
            return Character.toString(line.charAt(0));
        } */
    }
}
