import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import tokens.Tokens;
import tokens.Number;

import static tokens.Tokens.isMemberOfTokensMap;

public class Main {

    public static void main(String[] args) {
        List<String> lexems = new ArrayList<String>();
        Tokens tok = new Tokens();
        String vyraz = "CONST\n" +
                "  m =  7,\n" +
                "  n = 85;\n" +
                "\n" +
                "VAR\n" +
                "  x, y, z, q, r;\n" +
                "\n" +
                "PROCEDURE multiply;\n" +
                "VAR a, b;\n" +
                "\n" +
                "BEGIN\n" +
                "  a := x;\n" +
                "  b := y;\n" +
                "  z := 0;\n" +
                "  WHILE b > 0 DO BEGIN\n" +
                "    IF ODD b THEN z := z + a;\n" +
                "    a := 2 * a;\n" +
                "    b := b / 2\n" +
                "  END\n" +
                "END;\n" +
                "\n" +
                "PROCEDURE divide;\n" +
                "VAR w;\n" +
                "BEGIN\n" +
                "  r := x;\n" +
                "  q := 0;\n" +
                "  w := y;\n" +
                "  WHILE w <= r DO w := 2 * w;\n" +
                "  WHILE w > y DO BEGIN\n" +
                "    q := 2 * q;\n" +
                "    w := w / 2;\n" +
                "    IF w <= r THEN BEGIN\n" +
                "      r := r - w;\n" +
                "      q := q + 1\n" +
                "    END\n" +
                "  END\n" +
                "END;\n" +
                "\n" +
                "PROCEDURE gcd;\n" +
                "VAR f, g;\n" +
                "BEGIN\n" +
                "  f := x;\n" +
                "  g := y;\n" +
                "  WHILE f # g DO BEGIN\n" +
                "    IF f < g THEN g := g - f;\n" +
                "    IF g < f THEN f := f - g\n" +
                "  END;\n" +
                "  z := f\n" +
                "END;\n" +
                "\n" +
                "BEGIN\n" +
                "  x := m;\n" +
                "  y := n;\n" +
                "  CALL multiply;\n" +
                "  x := 25;\n" +
                "  y :=  3;\n" +
                "  CALL divide;\n" +
                "  x := 84;\n" +
                "  y := 36;\n" +
                "  CALL gcd\n" +
                "END;";
        vyraz = vyraz.replaceAll("\n", " ");
        for (String lexem : vyraz.split(" ")) {
            if (!lexem.trim().isEmpty()) {
                if (!lexem.trim().isEmpty()) {
                    if (tok.isMemberOfCharTokenMap(lexem)) {
                        String character = tok.getCharFromCharTokenMap(lexem);
                        lexems.add(lexem.replaceAll(character, " "));
                        lexems.add(character);
                    } else {
                        lexems.add(lexem);
                    }
                }
            }
        }


        System.out.println("-------------------------------------------\nNalezene lexemy:\n-------------------------------------------");
        Collections.reverse(lexems);
        for (int i = 0; i < lexems.size(); i++) {
            boolean found = false;
            if(tok.getCharToken(lexems.get(i).trim()) != null) {
                found = true;
                System.out.println(lexems.get(i).trim() + " - " + tok.getCharToken(lexems.get(i).trim()));
            } else if(tok.getToken(lexems.get(i).trim()) != null) {
                found = true;
                System.out.println(lexems.get(i).trim() + " - " + tok.getToken(lexems.get(i).trim()));
            }

            if(!found) {
                if(Number.isNumber(lexems.get(i))){
                    System.out.println(lexems.get(i).trim() + " - NUMBER");
                } else {
                    if(isMemberOfTokensMap(lexems.get(i+1))) {
                        System.out.println(lexems.get(i).trim() + " - IDENT");
                    } else {
                        System.out.println(lexems.get(i) + "- NONE");
                    }
                }
            }
        }
    }
}
